const http = require("http");
const Todo = require("./controller");
const { getReqData } = require("./utils");

const PORT = process.env.port || 7777;

const server = http.createServer(async (request, response) => {
	// /api/todos : GET
    if (request.url === "/api/users" && request.method === "GET") {
        // get the todos.
        const todos = await new Todo().getTodos();
        // set the status code, and content-type
        response.writeHead(200, { "Content-Type": "application/json" });
        // send the data
        response.end(JSON.stringify(todos));
    }

    // /api/todos/:id : GET
    else if (request.url.match(/\/api\/users\/([0-9]+)/) && request.method === "GET") {
        try {
            // get id from url
            const id = request.url.split("/")[3];
            // get todo
            const todo = await new Todo().getTodo(id);
            // set the status code and content-type
            response.writeHead(200, { "Content-Type": "application/json" });
            // send the data
            response.end(JSON.stringify(todo));
        } catch (error) {
            // set the status code and content-type
            response.writeHead(404, { "Content-Type": "application/json" });
            // send the error
            response.end(JSON.stringify({ message: error }));
        }
    }

    // /api/todos/:id : DELETE
    else if (request.url.match(/\/api\/todos\/([0-9]+)/) && request.method === "DELETE") {
        try {
            // get the id from url
            const id = request.url.split("/")[3];
            // delete todo
            let message = await new Todo().deleteTodo(id);
            // set the status code and content-type
            response.writeHead(200, { "Content-Type": "application/json" });
            // send the message
            response.end(JSON.stringify({ message }));
        } catch (error) {
            // set the status code and content-type
            response.writeHead(404, { "Content-Type": "application/json" });
            // send the error
            response.end(JSON.stringify({ message: error }));
        }
    }

    // /api/todos/:id : UPDATE
    else if (request.url.match(/\/api\/todos\/([0-9]+)/) && request.method === "PATCH") {
        try {
            // get the id from the url
            const id = request.url.split("/")[3];
            // update todo
            let updated_todo = await new Todo().updateTodo(id);
            // set the status code and content-type
            response.writeHead(200, { "Content-Type": "application/json" });
            // send the message
            response.end(JSON.stringify(updated_todo));
        } catch (error) {
            // set the status code and content type
            response.writeHead(404, { "Content-Type": "application/json" });
            // send the error
            response.end(JSON.stringify({ message: error }));
        }
    }

    // /api/todos/ : POST
    else if (request.url === "/api/todos" && request.method === "POST") {
        // get the data sent along
        let todo_data = await getReqData(req);
        // create the todo
        let todo = await new Todo().createTodo(JSON.parse(todo_data));
        // set the status code and content-type
        response.writeHead(200, { "Content-Type": "application/json" });
        //send the todo
        response.end(JSON.stringify(todo));
    }

    else {
        response.writeHead(404, { "Content-Type": "application/json" });
        response.end(JSON.stringify({ message: "Route not found" }));
    }
});

server.listen(PORT, () => {
	console.log(`server started on port: ${PORT}`);
});