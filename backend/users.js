var mysql = require('mysql');

console.log('Creating Connection');
var connection = mysql.createConnection({
    host: process.env.DATABASE_HOST || '127.0.0.1',
    user: 'root',
    password: '1234',
    database: 'tht',
    port: 3306
}); 

connection.connect(function(err) {
    if (err) {
        console.error("error connecting " + err.stack);
        return;
    }
    console.log('Database is connected successfully !');
});

connection.query('SELECT * FROM users',function(err,rows){
    if(err) {
        console.log(err);
    } else {
        console.log(rows);
    }
    connection.end();
});

const todos = [
    {
        id: 1,
        title: "Coding in Javascript",
        description: "Working with functions in JavaScript",
        completed: false,
    },
    {
        id: 2,
        title: "Cooking Supper",
        description: "Preparing rice and chicken",
        completed: false,
    },
    {
        id: 3,
        title: "Taking a walk",
        description: "Easy time at the park",
        completed: false,
    },
    {
        id: 4,
        title: "Watching Netflix",
        description: "Enjoying the new premiered series",
        completed: false,
    },
];
module.exports = todos;